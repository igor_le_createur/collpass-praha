import React from 'react'
import { TopAttractions } from '../components/TopAttractions'
import { Benefits } from '../components/Benefits'
import { Offers } from '../components/Offers'
import { HowToUse } from '../components/HowToUse'
import { NewsContainer } from '../components/LatesNews/NewsContainer'
import { CarouselSlider } from '../components/CarouselSlider'
import { Card } from '../components/Cards/Card'
import { Reviews } from '../components/Reviews/Reviews'
export const Home = () => {
  return (
    <div>
      <CarouselSlider />
      <TopAttractions />
      <Benefits />
      <Offers />
      <HowToUse />
      <NewsContainer />
      <Card />
      <Reviews />
    </div>
  )
}
