import { fetchTranslationData } from '../../api/api'

export const SET_LANGUAGE = 'SET_LANGUAGE'
export const SET_TRANSLATIONS = 'SET_TRANSLATIONS'

export const setLanguage = (language) => ({
  type: SET_LANGUAGE,
  language,
})

export const setTranslations = (translations) => ({
  type: SET_TRANSLATIONS,
  translations,
})

export const fetchTranslations = () => {
  return async (dispatch) => {
    try {
      const translationsData = await fetchTranslationData()

      dispatch(setTranslations(translationsData))
    } catch (error) {
      console.error('Error fetching translations', error)
    }
  }
}
