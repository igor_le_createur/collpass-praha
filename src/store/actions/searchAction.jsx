import { fetchAttractionsData } from '../../api/api'

export const SET_SEARCH_QUERY = 'SET_SEARCH_QUERY'
export const SET_SEARCH_RESULTS = 'SET_SEARCH_RESULTS'
export const SEARCH_REQUEST = 'SEARCH_REQUEST'
export const SEARCH_FAILURE = 'SEARCH_FAILURE'
export const SET_SELECTED_ATTRACTION = 'SET_SELECTED_ATTRACTION'

export const setSearchQuery = (query) => ({
  type: SET_SEARCH_QUERY,
  payload: query,
})

export const setSearchResults = (results) => ({
  type: SET_SEARCH_RESULTS,
  payload: results,
})

export const searchRequest = () => ({
  type: SEARCH_REQUEST,
})

export const searchFailure = (error) => ({
  type: SEARCH_FAILURE,
  payload: error,
})

export const setSelectedAttraction = (attraction) => ({
  type: SET_SELECTED_ATTRACTION,
  payload: attraction,
})

export const fetchSearchResults = () => {
  return async (dispatch) => {
    dispatch(searchRequest())
    try {
      const attractionsData = await fetchAttractionsData()
      const data = attractionsData.map((item) => ({
        ...item,
        slug: item.slug,
      }))
      dispatch(setSearchResults(data))
    } catch (error) {
      dispatch(searchFailure(error))
    }
  }
}
