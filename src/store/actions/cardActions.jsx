import { fetchCardCategoriesData } from '../../api/api'

export const FETCH_CARD_PRICES_REQUEST = 'FETCH_CARD_PRICES_REQUEST'
export const FETCH_CARD_PRICES_SUCCESS = 'FETCH_CARD_PRICES_SUCCESS'
export const FETCH_CARD_PRICES_FAILURE = 'FETCH_CARD_PRICES_FAILURE'

export const INCREMENT_PRICE = 'INCREMENT_PRICE'
export const DECREMENT_PRICE = 'DECREMENT_PRICE'

export const setCardPricesRequest = () => ({
  type: FETCH_CARD_PRICES_REQUEST,
})

export const setCardPricesSuccess = (data) => ({
  type: FETCH_CARD_PRICES_SUCCESS,
  payload: data.cards,
})

export const fetchCardPricesFailure = (error) => ({
  type: FETCH_CARD_PRICES_FAILURE,
  payload: error,
})

export const fetchCardPrices = () => {
  return (dispatch) => {
    dispatch(setCardPricesRequest())

    fetchCardCategoriesData()
      .then((data) => {
        dispatch(setCardPricesSuccess(data))
      })
      .catch((error) => {
        console.error('error while fetching data', error)
        dispatch(fetchCardPricesFailure(error))
      })
  }
}

export const incrementPrice = (cardId, productId, amount) => ({
  type: INCREMENT_PRICE,
  cardId,
  productId,
  amount,
})

export const decrementPrice = (cardId, productId, amount) => ({
  type: DECREMENT_PRICE,
  cardId,
  productId,
  amount,
})
