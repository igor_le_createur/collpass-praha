import { SET_LANGUAGE, SET_TRANSLATIONS } from '../actions/languageAction'

const initialState = {
  language: 'en',
  translations: {},
}

const languageReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_LANGUAGE:
      return {
        ...state,
        language: action.language,
      }
    case SET_TRANSLATIONS:
      return {
        ...state,
        translations: action.translations,
      }
    default:
      return state
  }
}

export default languageReducer
