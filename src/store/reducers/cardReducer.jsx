import {
  FETCH_CARD_PRICES_REQUEST,
  FETCH_CARD_PRICES_SUCCESS,
  FETCH_CARD_PRICES_FAILURE,
  INCREMENT_PRICE,
  DECREMENT_PRICE,
} from '../actions/cardActions'

const initialState = {
  cardPrices: [],
  currentPrices: {},
  cardSummaries: {},
  isLoading: false,
  error: null,
}

const cardReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_CARD_PRICES_REQUEST:
      return {
        ...state,
        isLoading: true,
        error: null,
      }
    case FETCH_CARD_PRICES_SUCCESS:
      return {
        ...state,
        cardPrices: action.payload,
        currentPrices: action.payload.reduce((acc, card) => {
          acc[card.id] = {}
          card.products.forEach((product) => {
            acc[card.id][product.id] = 0
          })
          return acc
        }, {}),
        cardSummaries: action.payload.reduce((acc, card) => {
          acc[card.id] = 0
          return acc
        }, {}),
        isLoading: false,
      }
    case FETCH_CARD_PRICES_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.payload,
      }
    case INCREMENT_PRICE:
      return {
        ...state,
        currentPrices: {
          ...state.currentPrices,
          [action.cardId]: {
            ...state.currentPrices[action.cardId],
            [action.productId]:
              state.currentPrices[action.cardId][action.productId] +
              action.amount,
          },
        },
        cardSummaries: {
          ...state.cardSummaries,
          [action.cardId]: state.cardSummaries[action.cardId] + action.amount,
        },
      }
    case DECREMENT_PRICE:
      return {
        ...state,
        currentPrices: {
          ...state.currentPrices,
          [action.cardId]: {
            ...state.currentPrices[action.cardId],
            [action.productId]:
              state.currentPrices[action.cardId][action.productId] -
              action.amount,
          },
        },
        cardSummaries: {
          ...state.cardSummaries,
          [action.cardId]: state.cardSummaries[action.cardId] - action.amount,
        },
      }
    default:
      return state
  }
}

export default cardReducer
