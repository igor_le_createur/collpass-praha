import { combineReducers } from 'redux'
import cardReducer from './cardReducer'
import languageReducer from './languageReducer'
import searchReducer from './searchReducer'

export const rootReducer = combineReducers({
  card: cardReducer,
  language: languageReducer,
  search: searchReducer,
})
