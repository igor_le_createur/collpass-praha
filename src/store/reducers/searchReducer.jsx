import {
  SET_SEARCH_QUERY,
  SET_SEARCH_RESULTS,
  SEARCH_REQUEST,
  SEARCH_FAILURE,
  SET_SELECTED_ATTRACTION,
} from '../actions/searchAction'

const initialState = {
  searchQuery: '',
  searchResults: [],
  allSearchResults: [],
  selectedAttraction: null,
  loading: false,
  error: null,
}

const searchReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_SEARCH_QUERY:
      return {
        ...state,
        searchQuery: action.query,
      }
    case SET_SEARCH_RESULTS:
      return {
        ...state,
        searchResults: action.payload,
        loading: false,
        error: null,
      }
    case SEARCH_REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      }
    case SEARCH_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.error,
      }
    case SET_SELECTED_ATTRACTION:
      return {
        ...state,
        selectedAttraction: action.payload,
      }
    default:
      return state
  }
}

export default searchReducer
