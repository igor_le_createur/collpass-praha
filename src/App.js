import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import './App.css';
import { Home } from './pages/Home';
import { NavBar } from './components/Header/NavBar';
import { Footer } from './components/Footer';
import { Provider } from 'react-redux';
import { store } from './store';
import { setLanguage } from './store/actions/languageAction';


const selectedLang = localStorage.getItem('selectedLang');

if (selectedLang) {
  store.dispatch(setLanguage(selectedLang));
}

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <Router>
          <NavBar />
          <Routes>
            <Route path='/' element={<Home />}></Route>
          </Routes>
          <Footer />
        </Router>

      </div>
    </Provider>

  );
}

export default App;
