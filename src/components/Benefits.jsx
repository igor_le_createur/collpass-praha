import React, { useState, useEffect } from 'react'
import { Collapse } from 'react-collapse'
import './Benefits.css'
import iphoneImage from '../assets/mobile.png'
import card from '../assets/PragueCard.png'
import { useSelector } from 'react-redux'
import { useDispatch } from 'react-redux'
import { fetchTranslations } from '../store/actions/languageAction'
import { fetchBenefitsData } from '../api/api'

export const Benefits = () => {
  const [collapsed, setCollapsed] = useState([0])
  const [benefitsData, setBenefitsData] = useState([])
  const language = useSelector((state) => state.language.language)
  const translations = useSelector((state) => state.language.translations)
  const dispatch = useDispatch()

  useEffect(() => {
    fetchBenefitsData()
      .then((data) => {
        setBenefitsData(data)
      })
      .catch((error) => {
        console.error('Error fetching data:', error)
      })
  }, [])

  useEffect(() => {
    dispatch(fetchTranslations)
  }, [dispatch])

  const toggleCollapse = (index) => {
    return setCollapsed(collapsed.filter((item) => item !== index))
      ? collapsed.includes(index)
      : setCollapsed([index])
  }

  return (
    <div className="benefits-container">
      <div className="left-side">
        <h3 className="benefits-section-title">
          {translations[language]?.HOME_benefits_title}
        </h3>
        {benefitsData.content?.[language]?.benefits.items
          .filter((benefit) => benefit.text && benefit.text.trim() !== '')
          .map((benefits, index) => (
            <div key={index} className="collapse-item">
              <div
                className="collapse-header"
                onClick={() => toggleCollapse(index)}
              >
                <h3 className="benefits-title">{benefits.title}</h3>
              </div>
              <Collapse isOpened={collapsed.includes(index)}>
                <div
                  className="collapse-content"
                  dangerouslySetInnerHTML={{ __html: benefits.text }}
                ></div>
              </Collapse>
            </div>
          ))}
      </div>
      <div className="right-side">
        <div className="phone">
          <img src={iphoneImage} alt="iPhone" />
        </div>
        <div className="coolpass-card">
          <img src={card} alt="coolpass-card" />
        </div>
      </div>
    </div>
  )
}
