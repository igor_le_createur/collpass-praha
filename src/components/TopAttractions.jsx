import React, { useState, useEffect } from 'react'
import Slider from 'react-slick'
import './TopAttractions.css'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import { useDispatch, useSelector } from 'react-redux'
import { fetchTranslations } from '../store/actions/languageAction'
import { fetchTopAttractionsData } from '../api/api'
import useIsMobile from '../hooks/IsMobile'

export const TopAttractions = () => {
  const [attractions, setAttractions] = useState([])
  const language = useSelector((state) => state.language.language)
  const translations = useSelector((state) => state.language.translations)
  const dispatch = useDispatch()
  const isMobile = useIsMobile()

  const settings = {
    slidesToShow: isMobile ? 1 : 4,
    infinite: true,
    arrows: true,
  }

  useEffect(() => {
    fetchTopAttractionsData()
      .then((data) => {
        setAttractions(data)
      })
      .catch((error) => {
        console.error('Error fetching data:', error)
      })
  }, [])

  useEffect(() => {
    dispatch(fetchTranslations)
  }, [dispatch])

  return (
    <div className="top-attractions-container">
      <h3 className="top-attractions-title">
        {translations[language]?.HOME_top_attractions_title}
      </h3>
      <div className="carousel-container">
        <Slider className="custom-slider" {...settings}>
          {attractions.map((attraction, index) => (
            <div key={index} className="card-wrapper">
              <div className="card">
                <div className="benefit">
                  <text>
                    {attraction.benefit === '- 15%' ? (
                      <>
                        {attraction.benefit}{' '}
                        {translations[language]?.ATTRACTIONS_label_with_pass}
                      </>
                    ) : (
                      <>
                        {translations[language]?.ATTRACTIONS_label_included}{' '}
                        {translations[language]?.ATTRACTIONS_label_with_pass}
                      </>
                    )}
                  </text>
                </div>
                <img
                  className="top-attractions-pic"
                  src={`https://static2.praguecoolpass.com/${
                    isMobile ? attraction.images[0] : attraction.webimages[0]
                  }`}
                  alt="Attraction"
                />
                <div className="footer-top-attractions">
                  <h4>{attraction.content[language].title}</h4>
                  <p>{attraction.content[language].subtitle}</p>
                </div>
              </div>
            </div>
          ))}
        </Slider>
      </div>
    </div>
  )
}
