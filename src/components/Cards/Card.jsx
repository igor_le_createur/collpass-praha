import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {
  fetchCardPrices,
  incrementPrice,
  decrementPrice,
} from '../../store/actions/cardActions'
import Slider from 'react-slick'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import './Card.css'
import useIsMobile from '../../hooks/IsMobile'

export const Card = () => {
  const dispatch = useDispatch()
  const cardPrices = useSelector((state) => state.card.cardPrices)
  const currentPrices = useSelector((state) => state.card.currentPrices)
  const language = useSelector((state) => state.language.language)
  const translations = useSelector((state) => state.language.translations)
  const [defaultCounts, setDefaultCounts] = useState({})
  const [cardSummaries, setCardSummaries] = useState({})
  const isMobile = useIsMobile()

  const settings = {
    slidesToShow: isMobile ? 1 : 3,
    infinite: true,
    arrows: true,
    dots: isMobile ? true : false,
  }

  useEffect(() => {
    dispatch(fetchCardPrices())
  }, [dispatch])

  useEffect(() => {
    const initialCounts = {}
    cardPrices.forEach((card) => {
      card.products.forEach((product) => {
        initialCounts[product.name.default] = 0
      })
    })
    setDefaultCounts(initialCounts)
  }, [cardPrices])

  useEffect(() => {
    const newCardSummaries = {}

    cardPrices.forEach((card) => {
      const cardId = card.id
      const summary = card.products.reduce((total, product) => {
        const productId = product.id
        const count =
          currentPrices[cardId] && currentPrices[cardId][productId]
            ? currentPrices[cardId][productId]
            : 0
        return total + count
      }, 0)

      newCardSummaries[cardId] = summary
    })

    setCardSummaries(newCardSummaries)
  }, [currentPrices, cardPrices])

  const trackIncrement = (cardId, productId, amount, defaultName) => {
    dispatch(incrementPrice(cardId, productId, amount))
    setDefaultCounts((prevCounts) => ({
      ...prevCounts,
      [defaultName]: (prevCounts[defaultName] || 0) + 1,
    }))
  }

  const trackDecrement = (cardId, productId, amount, defaultName) => {
    if (currentPrices[cardId] && currentPrices[cardId][productId] > 0) {
      dispatch(decrementPrice(cardId, productId, amount))
      setDefaultCounts((prevCounts) => ({
        ...prevCounts,
        [defaultName]: (prevCounts[defaultName] || 0) - 1,
      }))
    }
  }

  return (
    <div className="cards-container">
      <h3 className="buy-card-title">
        {translations[language]?.BUY_COOLPASS_PRAGUE_CARD}
      </h3>
      <Slider {...settings} className="slider-card-container">
        {cardPrices.map((card) => (
          <div className="buy-card-container" key={card.id}>
            <div className="buy-card-header">
              <h3 className="buy-card-h3">{card.name.default}</h3>
              <p className="buy-card-subtitle">
                {translations[language]?.BUY_PRAGUE_CARD_COOL_PASS}
              </p>
            </div>
            <div className="product-table">
              <div className="product-content">
                <div className="price-text">
                  {translations[language]?.PRICE}
                </div>
                {card.products.slice(0, 2).map((product) => (
                  <div key={product.id} className="product-row">
                    <div className="name-label">
                      {product.name.default.slice(6, 11)}
                    </div>
                    <div className="price-label">{product.price} EUR</div>
                    <div className="product-actions">
                      <button
                        onClick={() =>
                          trackIncrement(
                            card.id,
                            product.id,
                            product.price,
                            product.name.default
                          )
                        }
                        className="increment-button"
                      ></button>
                      <div className="counter-container">
                        <div className="count-label">
                          {defaultCounts[product.name.default]}
                        </div>
                      </div>

                      <button
                        onClick={() =>
                          trackDecrement(
                            card.id,
                            product.id,
                            product.price,
                            product.name.default
                          )
                        }
                        className="decrement-button"
                      ></button>
                    </div>
                  </div>
                ))}
                <div className="price-wrapped">
                  <p className="price-summary">
                    {translations[language]?.YOUR_PRICE}
                  </p>
                  <p className="price-itself">{cardSummaries[card.id]} EUR</p>
                </div>
              </div>
            </div>
            <div className="buy-card-footer">
              <a className="buy-card-footer-text">
                {translations[language]?.CALCULATOR_COMPLETE_BOOKING_BTN}
              </a>
            </div>
          </div>
        ))}
      </Slider>
      <div className="card-tips">
        <div className="frst-card-tip">
          <ul className="card-list">
            <li className="text-tips">
              {translations[language]?.CALCULATOR_card_validity}
            </li>
            <li className="text-tips">
              {translations[language]?.CALCULATOR_child_card_validity_tip}
            </li>
          </ul>
        </div>
        <div className="scnd-card-tip">
          <ul className="card-list">
            <li className="text-tips">
              {translations[language]?.CALCULATOR_student_id_info}
            </li>
          </ul>
        </div>
        <div className="third-card-tip">
          <p>{translations[language]?.ADULT_AGE}</p>
          <p>{translations[language]?.STUDENT_AGE}</p>
          <p>{translations[language]?.CHILD_AGE}</p>
        </div>
      </div>
    </div>
  )
}
