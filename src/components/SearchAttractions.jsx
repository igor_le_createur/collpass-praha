import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import './SearchAttractions.css'
import ReactSearchBox from 'react-search-box'
import {
  setSearchQuery,
  fetchSearchResults,
  setSelectedAttraction,
} from '../store/actions/searchAction'

export const SearchAttractions = () => {
  const language = useSelector((state) => state.language.language)
  const translations = useSelector((state) => state.language.translations)
  const searchQuery = useSelector((state) => state.search.searchQuery)
  const results = useSelector((state) => state.search.searchResults)
  const showErrorMessage = useSelector(
    (state) =>
      state.search.searchQuery && state.search.searchQuery.trim() !== ''
  )
  const selectedAttraction = useSelector(
    (state) => state.search.selectedAttraction
  )
  console.log('Selected Attraction:', selectedAttraction)

  const dispatch = useDispatch()

  const handleSearch = (query) => {
    if (query && query.trim().length >= 3) {
      dispatch(setSearchQuery(query))
      dispatch(fetchSearchResults(query, language))
    }
  }
  const handleAttractionSelect = (record) => {
    dispatch(setSelectedAttraction(record))
    console.log('Selected Attraction Dispatched:', record)
  }

  return (
    <div>
      <label htmlFor="searchQuery"></label>
      <ReactSearchBox
        dropDownBorderColor="#fdca2e"
        placeholder={translations[language]?.SEARCH}
        value={searchQuery}
        data={results.map((result) => ({
          key: result._id,
          value: result.content[language].title,
          url: result.slug,
        }))}
        onSelect={(record) => handleAttractionSelect(record)}
        fuseConfigs={{
          threshold: 0.3,
          keys: ['value'],
        }}
        onChange={(newValue) => {
          handleSearch(newValue)
        }}
        showDropDown={results.length > 0 && searchQuery !== ''}
      />
      {showErrorMessage && results.length === 0 && (
        <div className="error-message">Sorry, can't find it</div>
      )}
    </div>
  )
}
