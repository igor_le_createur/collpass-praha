import React, { useEffect } from 'react'
import { Row, Col, Form, FormControl, Button } from 'react-bootstrap'
import './Footer.css'
import { useSelector } from 'react-redux'
import { useDispatch } from 'react-redux'
import { fetchTranslations } from '../store/actions/languageAction'
import appstoreImage from '../assets/appstore.png'
import googleplayImage from '../assets/googleplay.png'

export const Footer = () => {
  const language = useSelector((state) => state.language.language)
  const translations = useSelector((state) => state.language.translations)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(fetchTranslations)
  }, [dispatch])
  return (
    <footer className="footer">
      <Row className="footer-content">
        <Col className="frst-footer-section">
          <ul className="footer-links">
            <a href="#">{translations?.en?.FOOTER_pass_and_card}</a>
            <a href="#">{translations[language]?.FOOTER_USING_COOLPASS}</a>
            <a href="#">{translations[language]?.FOOTER_how_you_save}</a>
            <a href="#">{translations[language]?.FOOTER_get_your_pass}</a>
            <a href="#">{translations[language]?.FOOTER_sales_points}</a>
            <a href="#">{translations[language]?.FOOTER_reviews}</a>
          </ul>
        </Col>
        <Col className="scnd-footer-section">
          <ul className="footer-links">
            <a href="#">{translations[language]?.ATTRACTIONS}</a>
            <a href="#">{translations[language]?.FOOTER_sightseeing_tours}</a>
            <a href="#">{translations[language]?.FOOTER_areas}</a>
            <a href="#">{translations[language]?.FOOTER_closures}</a>
            <a href="#">{translations[language]?.FOOTER_whats_on}</a>
            <a href="#">{translations[language]?.FOOTER_contact_us}</a>
          </ul>
        </Col>
        <Col className="download-links">
          <ul className="footer-links">
            <Button className="sub-faq" variant="primary">
              {translations[language]?.FOOTER_faq}
            </Button>
            <a href="#">{translations[language]?.FOOTER_about_us}</a>
            <a href="#">
              {translations[language]?.FOOTER_terms_and_conditions}
            </a>
            <a href="#">
              {translations[language]?.FOOTER_cancellation_and_refund}
            </a>
            <a href="#">{translations[language]?.FOOTER_privacy_policy}</a>
          </ul>
        </Col>
        <Col className="about-us-footer-section">
          <ul className="footer-links">
            <text className="download-btn">
              {translations[language]?.DOWNLOAD}
            </text>
            <text className="download-app-title">
              {translations[language]?.FOOTER_prague_coolpass_app}
            </text>
            <div className="download-images">
              <img
                src={appstoreImage}
                alt="App Store"
                className="appstore-image"
              />
              <img
                src={googleplayImage}
                alt="Google Play"
                className="googleplay-image"
              />
            </div>
          </ul>
        </Col>
        <Col className="news-and-updates">
          <div className="news-and-updates-title">
            {translations[language]?.NEWS_AND_UPDATES}
          </div>
          <div className="footer-links">
            <Form inline>
              <FormControl
                className="sub-input-form"
                type="text"
                placeholder={translations[language]?.ENTER_EMAIL_PLACEHOLDER}
              />
              <Button className="sub-btn">
                {translations[language]?.SUBSCRIBE_BUTTON}
              </Button>
            </Form>
            <div className="footer-info-year">
              CoolPass 2020-{new Date().getFullYear()}
            </div>
            <div className="footer-info-year">
              Prague Card 1992-{new Date().getFullYear()}
            </div>
            <div className="rights-reserved-block">
              {translations[language]?.ALL_RIGHTS_RESERVED_LABEL}
            </div>
          </div>
        </Col>
      </Row>
    </footer>
  )
}
