import React, { useEffect, useState } from 'react'
import { Carousel } from 'react-responsive-carousel'
import 'react-responsive-carousel/lib/styles/carousel.min.css'
import './CarouselSlider.css'
import axios from 'axios'
import { useSelector } from 'react-redux'
import { SearchAttractions } from './SearchAttractions'
import { SearchButton } from './SearchButton'
import useIsMobile from '../hooks/IsMobile'
import { fetchBenefitsData } from '../api/api'

export const CarouselSlider = () => {
  const [dataCarousel, setDataCarousel] = useState([])
  const language = useSelector((state) => state.language.language)
  const isMobile = useIsMobile()

  useEffect(() => {
    fetchBenefitsData()
      .then((data) => {
        setDataCarousel(data)
      })
      .catch((error) => {
        console.error('Error fetching data:', error)
      })
  }, [])

  return (
    <div className="carousel-container">
      <Carousel
        autoPlay={true}
        interval={5000}
        infiniteLoop={true}
        showThumbs={false}
      >
        {dataCarousel.mainImage &&
          (isMobile
            ? dataCarousel.mainImage.app_image
            : dataCarousel.mainImage.web_image
          ).map((image, index) => (
            <div key={index} className="carousel-slider-container">
              <div className="image-container">
                <img
                  src={`https://static2.praguecoolpass.com/${image}`}
                  alt={`Image ${index + 1}`}
                />
              </div>
            </div>
          ))}
      </Carousel>
      <div className="text-container">
        <h1 className="header-title">
          {dataCarousel.content?.[language].title}
        </h1>
        <h3 className="header-subtitle">
          {dataCarousel.content?.[language].subtitle}
        </h3>
        <div className="search-container">
          {isMobile ? null : <SearchAttractions />}
          {isMobile ? null : <SearchButton />}
        </div>
      </div>
      <div className="underslider">
        <p className="underslider-text">
          {dataCarousel.content?.[language].header_banner}
        </p>
      </div>
      <div className="search-container">
        {isMobile && <SearchAttractions />}
        {isMobile && <SearchButton />}
      </div>
    </div>
  )
}
