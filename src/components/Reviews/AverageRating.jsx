import React, { useEffect, useState } from 'react'
import './Reviews.css'
import axios from 'axios'

export const AverageRating = () => {
  const [reviewsData, setReviewsData] = useState([])
  const [averageRating, setAverageRating] = useState(0)

  useEffect(() => {
    axios
      .get('https://api2.praguecoolpass.com/review/approved')
      .then((response) => {
        setReviewsData(response.data)
        const totalRating = response.data.reduce(
          (sum, review) => sum + review.rating,
          0
        )
        const average = totalRating / response.data.length
        setAverageRating(average)
      })
      .catch((error) => {
        error.log('error while fetching data', error)
      })
  }, [])

  return (
    <div className="rating-container">
      <div className="average-rating">{averageRating.toFixed(1)}</div>
      <div className="rating">
        {Array.from({ length: 5 }).map((_, index) => (
          <span
            key={index}
            className={`star ${
              index < Math.floor(averageRating) ? 'filled-star' : ''
            }`}
          >
            &#9733;
          </span>
        ))}
      </div>
    </div>
  )
}
