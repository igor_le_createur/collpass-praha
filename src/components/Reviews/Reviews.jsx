import React, { useState, useEffect } from 'react'
import Slider from 'react-slick'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import axios from 'axios'
import { useSelector } from 'react-redux'
import { useDispatch } from 'react-redux'
import { fetchTranslations } from '../../store/actions/languageAction'
import './Reviews.css'
import { AverageRating } from './AverageRating'
import { format } from 'date-fns'
import useIsMobile from '../../hooks/IsMobile'

export const Reviews = () => {
  const [reviewsData, setReviewsData] = useState([])
  const [showFullText, setShowFullText] = useState(
    Array(reviewsData.length).fill(false)
  )
  const language = useSelector((state) => state.language.language)
  const translations = useSelector((state) => state.language.translations)
  const dispatch = useDispatch()
  const [sliderFilledStars, setSliderFilledStars] = useState([])
  const [sliderEmptyStars, setSliderEmptyStars] = useState([])
  const isMobile = useIsMobile()
  const [currentSlide, setCurrentSlide] = useState(0)

  const settings = {
    slidesToShow: isMobile ? 1 : 3,
    infinite: true,
    arrows: true,
    beforeChange: (oldIndex, newIndex) => {
      setCurrentSlide(newIndex)
    },
  }

  useEffect(() => {
    axios
      .get('https://api2.praguecoolpass.com/review/approved')
      .then((response) => {
        setReviewsData(response.data)
        const sliderStars = response.data.map((review) => ({
          filled: Array.from({ length: review.rating }),
          empty: Array.from({ length: 5 - review.rating }),
        }))
        setSliderFilledStars(sliderStars.map((stars) => stars.filled))
        setSliderEmptyStars(sliderStars.map((stars) => stars.empty))
      })

      .catch((error) => {
        error.log('error while fetching data', error)
      })
  }, [])

  useEffect(() => {
    dispatch(fetchTranslations)
  }, [dispatch])

  return (
    <div className="review-card-container">
      <div className="review-card-header">
        <h3 className="review-card-title">
          {translations[language]?.REVIEWS_what_do_customers_say}
        </h3>
        <AverageRating />
      </div>
      <Slider {...settings}>
        {reviewsData.map((review, index) => (
          <div className="review-slider-card-container" key={review._id}>
            <div className="review-card">
              <div className="rating">
                {sliderFilledStars[index].map((_, starIndex) => (
                  <span key={starIndex} className="star filled-star">
                    &#9733;
                  </span>
                ))}
                {sliderEmptyStars[index].map((_, starIndex) => (
                  <span key={starIndex} className="star">
                    &#9734;
                  </span>
                ))}
              </div>
              <div className="review-card-content">
                <div className="title-review">{review.title}</div>
                <p className="reviews-date-published">
                  {format(new Date(review.date), 'MMMM d, yyyy')}
                </p>
                <p className="review-text">
                  {review.text && (
                    <>
                      {showFullText[index] ? (
                        <>
                          {review.text}
                          <button
                            className="read-more-button"
                            onClick={() => {
                              const updatedShowFullText = [...showFullText]
                              updatedShowFullText[index] = !showFullText[index]
                              setShowFullText(updatedShowFullText)
                            }}
                          >
                            less
                          </button>
                        </>
                      ) : (
                        <>
                          {review.text.slice(0, 100)}...
                          {review.text.length > 100 && (
                            <button
                              className="read-more-button"
                              onClick={() => {
                                const updatedShowFullText = [...showFullText]
                                updatedShowFullText[index] =
                                  !showFullText[index]
                                setShowFullText(updatedShowFullText)
                              }}
                            >
                              more
                            </button>
                          )}
                        </>
                      )}
                    </>
                  )}
                </p>
              </div>
              <div className="review-footer">
                <div className="review-place">{review.place}</div>
              </div>
            </div>
          </div>
        ))}
      </Slider>
      <div className="pagination-info">
        <span className="current-slide">{currentSlide + 1}</span>
        {'/'}
        <span className="total-slides">{reviewsData.length}</span>
      </div>
    </div>
  )
}
