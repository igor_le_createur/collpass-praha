import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { setLanguage } from '../../store/actions/languageAction'
import './LanguageSelector.css'

export const LanguageSelector = () => {
  const dispatch = useDispatch()
  const [selectedLanguage, setSelectedLanguage] = useState(
    localStorage.getItem('selectedLang') || 'en'
  )

  useEffect(() => {
    const savedLanguage = localStorage.getItem('selectedLang')
    if (savedLanguage) {
      setSelectedLanguage(savedLanguage)
      dispatch(setLanguage(savedLanguage))
    }
  }, [dispatch])

  const handleChangeLanguage = (language) => {
    setSelectedLanguage(language)
    dispatch(setLanguage(language))
    localStorage.setItem('selectedLang', language)
  }
  return (
    <div className="language-selector">
      <select
        className="language-select"
        onChange={(e) => handleChangeLanguage(e.target.value)}
        value={selectedLanguage}
      >
        <option className="lang" value="en">
          ENGLISH
        </option>
        <option className="lang" value="de">
          DEUTSCH
        </option>
        <option className="lang" value="ru">
          РУССКИЙ
        </option>
        <option className="lang" value="fr">
          FRANÇAIS
        </option>
      </select>
    </div>
  )
}
