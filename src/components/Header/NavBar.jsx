import React, { useEffect, useState } from 'react'
import { BuyButton } from './BuyButton'
import axios from 'axios'
import './NavBar.css'
import { LanguageSelector } from './LanguageSelector'
import { useSelector } from 'react-redux'
import { MobileMenu } from './MobileMenu'
import useIsMobile from '../../hooks/IsMobile'
import 'react-burger-menu/lib/menus/fallDown.js'

export const NavBar = () => {
  const [menuItems, setMenuItems] = useState([])
  const language = useSelector((state) => state.language.language)
  const translations = useSelector((state) => state.language.translations)
  const [isNavbarVisible, setIsNavbarVisible] = useState(true)
  const [isMobileMenuOpen, setIsMobileMenuOpen] = useState(false)
  const isMobile = useIsMobile()

  const filteredTitles = [
    'COOLPASS/CARD',
    'PLAN YOUR TRIP',
    'GET YOUR PASS',
    'CURRENT NEWS',
    'FAQ',
    'ATTRACTIONS',
  ]

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get('https://api2.praguecoolpass.com/menu')
        const titleSet = new Set()
        const filteredMenuItems = response.data.filter((menuItem) => {
          if (filteredTitles.includes(menuItem.content.en.title)) {
            if (!titleSet.has(menuItem.content.en.title)) {
              titleSet.add(menuItem.content.en.title)
              return true
            }
          }
          return false
        })
        setMenuItems(filteredMenuItems)
      } catch (error) {
        console.error('error fetching  data:', error)
      }
    }

    fetchData()
  }, [])

  const handleScroll = () => {
    const currentScrollY = window.scrollY
    return currentScrollY > 0
      ? setIsNavbarVisible(false)
      : setIsNavbarVisible(true)
  }

  useEffect(() => {
    window.addEventListener('scroll', handleScroll)
    return () => {
      window.removeEventListener('scroll', handleScroll)
    }
  }, [])

  const mobileMenuStyles = {
    menuWrap: (isOpen) => {
      const styles = {
        MozTransform: isOpen
          ? 'translate3d(0, 0, 0)'
          : 'translate3d(0, 100%, 0)',
        MsTransform: isOpen
          ? 'translate3d(0, 0, 0)'
          : 'translate3d(0, 100%, 0)',
        OTransform: isOpen ? 'translate3d(0, 0, 0)' : 'translate3d(0, 100%, 0)',
        WebkitTransform: isOpen
          ? 'translate3d(0, 0, 0)'
          : 'translate3d(0, 100%, 0)',
        transform: isOpen ? 'translate3d(0, 0, 0)' : 'translate3d(0, 100%, 0)',
        transition: 'all 0.5s ease-in-out',
      }
      console.log('Styles:', styles)
      return styles
    },
  }

  return (
    <div
      className={`navigation ${
        isNavbarVisible ? 'navbar-visible' : 'navbar-hidden'
      }`}
    >
      <a href="/" className="left-nav">
        <a className="logo-title">CoolPass</a>
      </a>
      {isMobile ? (
        <div className="mobile-menu-container">
          <div
            className={`mobile-menu-icon ${isMobileMenuOpen ? 'open' : ''}`}
            onClick={() => setIsMobileMenuOpen(!isMobileMenuOpen)}
          >
            <span className="bar"></span>
            <span className="bar"></span>
            <span className="bar"></span>
          </div>
          <a href="/" className="left-nav">
            <a className="logo-title-mob">CoolPass</a>
          </a>
          {!isMobileMenuOpen ? (
            <div className="header-buy-btn">
              <BuyButton>{translations[language]?.BUY_NOW}</BuyButton>
            </div>
          ) : (
            <div className="header-buy-btn">
              <LanguageSelector />
            </div>
          )}
        </div>
      ) : (
        <div>
          <ul className="centre-nav">
            {menuItems.map((menuItem) => (
              <li key={menuItem._id}>
                <a href={menuItem.link}>{menuItem.content[language].title}</a>
              </li>
            ))}
          </ul>
        </div>
      )}
      {isMobile ? null : (
        <ul className="right-nav">
          <BuyButton>{translations[language]?.BUY_NOW}</BuyButton>
          <LanguageSelector />
        </ul>
      )}
      <MobileMenu
        isOpen={isMobileMenuOpen}
        onClose={() => setIsMobileMenuOpen(false)}
        onOpen={() => setIsMobileMenuOpen(true)}
        menuItems={menuItems}
        language={language}
        styles={mobileMenuStyles}
      />
    </div>
  )
}
