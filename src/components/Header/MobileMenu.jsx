import React from 'react'
import { fallDown as Menu } from 'react-burger-menu'
import { BuyButton } from './BuyButton'
import { useSelector } from 'react-redux'

export const MobileMenu = ({
  isOpen,
  onClose,
  onOpen,
  menuItems,
  language,
  styles,
}) => {
  const translations = useSelector((state) => state.language.translations)
  console.log(styles)

  return (
    <Menu
      isOpen={isOpen}
      onStateChange={(state) => {
        !state.isOpen ? onClose() : onOpen()
      }}
      style={styles.menuWrap(isOpen)}
    >
      {menuItems.map((menuItem) => (
        <a key={menuItem._id} className="menu-item" href={menuItem.link}>
          {menuItem.content[language].title}
        </a>
      ))}
      <div className="mobile-menu-buy-button">
        <BuyButton>{translations[language]?.BUY_NOW}</BuyButton>
      </div>
    </Menu>
  )
}
