import React from 'react'
import './BuyButton.css'

export const BuyButton = (props) => {
  return (
    <div>
      <button className="btn">{props.children}</button>
    </div>
  )
}
