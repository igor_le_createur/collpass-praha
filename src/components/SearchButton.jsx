import React from 'react'
import './SearchButton.css'
import { useSelector, useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'

export const SearchButton = () => {
  const selectedAttraction = useSelector(
    (state) => state.search.selectedAttraction
  )
  console.log('Selected Attraction:', selectedAttraction)

  const dispatch = useDispatch()
  const navigate = useNavigate()

  const handleSearchButtonClick = () => {
    navigate(`/attraction/${selectedAttraction.item.url}`)
  }

  return (
    <div>
      <button className="search-button" onClick={handleSearchButtonClick}>
        Let's go
      </button>
    </div>
  )
}
