import React, { useState, useEffect } from 'react'
import './Offers.css'
import { useSelector } from 'react-redux'
import { useDispatch } from 'react-redux'
import { fetchTranslations } from '../store/actions/languageAction'
import useIsMobile from '../hooks/IsMobile'
import { fetchPagesData } from '../api/api'

export const Offers = () => {
  const [offerData, setOfferData] = useState({})
  const language = useSelector((state) => state.language.language)
  const translations = useSelector((state) => state.language.translations)
  const dispatch = useDispatch()
  const isMobile = useIsMobile()

  useEffect(() => {
    fetchPagesData()
      .then((data) => {
        setOfferData(data)
      })
      .catch((error) => {
        console.error('Error fetching data:', error)
      })
  }, [])

  useEffect(() => {
    dispatch(fetchTranslations)
  }, [dispatch])

  return (
    <div className="container-offer">
      <h3>{translations[language]?.HOME_offers_title}</h3>
      <div className="content">
        {offerData.content?.[language].offers.items.map((offer, index) => (
          <div key={index} className="a-card">
            <div className="card-content">
              <img
                className="top-offer-pic"
                src={`https://static2.praguecoolpass.com/${
                  !isMobile
                    ? offerData?.offers.web_images[index]
                    : offerData?.offers.app_images[index]
                }`}
                alt="Offer"
              />
            </div>
            <div className="offer-btn">
              <div className="offer-title">{offer.title}</div>
            </div>
            <div className="offer-details">
              <div className="wrapped-offer-text">
                <p
                  className="pop-up-title"
                  dangerouslySetInnerHTML={{ __html: offer.title }}
                ></p>
                <p
                  className="pop-up-text"
                  dangerouslySetInnerHTML={{ __html: offer.features_list }}
                ></p>
              </div>

              <div className="wrapped-offer-btn">
                <button className="see-offer-btn">{offer.button_text}</button>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  )
}
