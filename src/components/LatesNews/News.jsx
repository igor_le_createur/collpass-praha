import React, { useState, useEffect } from 'react'
import './News.css'
import axios from 'axios'
import { useSelector, useDispatch } from 'react-redux'
import { fetchTranslations } from '../../store/actions/languageAction'

export const News = () => {
  const [newsData, setNewsData] = useState([])
  const translations = useSelector((state) => state.language.translations)
  const language = useSelector((state) => state.language.language)
  const dispatch = useDispatch()

  useEffect(() => {
    axios
      .get('https://api2.praguecoolpass.com/news')
      .then((response) => {
        setNewsData(response.data)
      })
      .catch((error) => {
        console.error('Error fetching data', error)
      })
  }, [])

  useEffect(() => {
    dispatch(fetchTranslations())
  }, [dispatch])

  const latestNews = newsData.slice(0, 2)

  const extractFirstTwoParagraphs = (htmlContent) => {
    const div = document.createElement('div')
    div.innerHTML = htmlContent
    const paragraphs = div.querySelectorAll('p')
    if (paragraphs.length >= 2) {
      return paragraphs[0].outerHTML + paragraphs[2].outerHTML
    }
    return htmlContent
  }

  return (
    <div>
      <div className="news-section">
        <h3 className="news-section-title">
          {translations[language]?.HOME_news_title}
        </h3>
        {latestNews.map((newsItem, index) => (
          <div key={index} className="news-item">
            <div className="image">
              <img
                src={`https://static2.praguecoolpass.com/${newsItem.images[0]}`}
                alt="News Image"
              />
              <div className="date-published">
                {new Date(newsItem.datePublished).toLocaleDateString('en-US')}
              </div>
            </div>
            <div className="text">
              <h4 className="news-title">{newsItem.content?.en?.title}</h4>
              <div
                className="news-text"
                dangerouslySetInnerHTML={{
                  __html: extractFirstTwoParagraphs(newsItem.content?.en?.text),
                }}
              />{' '}
              <a className="read-more-news" href="/">
                {translations[language]?.READ_MORE}
              </a>
            </div>
          </div>
        ))}
      </div>
    </div>
  )
}
