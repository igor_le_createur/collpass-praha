import React, { useState, useEffect } from 'react'
import './HowToUse.css'
import axios from 'axios'
import { useSelector } from 'react-redux'
import { useDispatch } from 'react-redux'
import { fetchTranslations } from '../store/actions/languageAction'
import useIsMobile from '../hooks/IsMobile'
import { Carousel } from 'react-responsive-carousel'
import { fetchPagesData } from '../api/api'

export const HowToUse = () => {
  const [usageData, setUsageData] = useState([])
  const language = useSelector((state) => state.language.language)
  const translations = useSelector((state) => state.language.translations)
  const dispatch = useDispatch()
  const isMobile = useIsMobile()

  useEffect(() => {
    fetchPagesData()
      .then((data) => {
        setUsageData(data)
      })
      .catch((error) => {
        console.error('Error fetching data:', error)
      })
  }, [])

  useEffect(() => {
    dispatch(fetchTranslations)
  })

  return (
    <div className="usage-section">
      <h3 className="usage-section-title">
        {translations[language]?.HOME_how_to_use_title}
      </h3>
      {isMobile ? (
        <Carousel>
          {usageData.content?.[language]?.how_to_use.descriptions?.map(
            (descriptions, index) => (
              <div key={index} className="usage-card">
                <div className="usage-image">
                  <img
                    src={`https://static2.praguecoolpass.com/${usageData?.how_to_use.app_images[index]}`}
                    alt={`Image ${index + 1}`}
                  />
                </div>
                <div className="order-num">
                  <p className="num-step">{index + 1}</p>
                </div>
                <div className="step-hint">{descriptions}</div>
              </div>
            )
          )}
        </Carousel>
      ) : (
        <div className="usage-container">
          {usageData.content?.[language]?.how_to_use.descriptions?.map(
            (descriptions, index) => (
              <div key={index} className="usage-card">
                <div className="usage-image">
                  <img
                    src={`https://static2.praguecoolpass.com/${usageData?.how_to_use.app_images[index]}`}
                  />
                </div>
                <div className="order-num">
                  <p className="num-step">{index + 1}</p>
                </div>
                <div className="step-hint">{descriptions}</div>
              </div>
            )
          )}
        </div>
      )}
    </div>
  )
}
