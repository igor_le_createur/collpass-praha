import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import {
    getFirestore,
    collection,
    addDoc,
    doc,
    getDoc,
    getDocs,
    setDoc,
    query,
    where,
    orderBy,
    limit,
    updateDoc,
    onSnapshot
} from 'firebase/firestore';
import { setLanguage } from "./store/actions/languageAction";


const firebaseConfig = {
    apiKey: "AIzaSyCOqyk1s6bWztXFCaf9J08OtxdTtlqiEiY",
    authDomain: process.env.REACT_APP_FIREBASE_AUTH_DOMAIN,
    projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
    storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
    messagingSenderId: process.env.REACT_APP_FIREBASE_MESSAGING_SENDER_ID,
    appId: process.env.REACT_APP_FIREBASE_APP_ID,
};


const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const db = getFirestore(app);
export const firestoreDoc = doc;
export const firestoreCollection = collection;
export const firestoreGetDoc = getDoc;
export const firestoreGetDocs = getDocs;
export const firestoreQuery = query;
export const firestoreWhere = where;
export const firestoreOrderBy = orderBy;
export const firestoreLimit = limit;
export const firestoreUpdateDoc = updateDoc;
export const firestoreOnSnapShot = onSnapshot;
export const firestoreAddDoc = addDoc;
export const firestoreSetDoc = setDoc;


export default app;
