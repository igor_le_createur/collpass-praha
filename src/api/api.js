import axios from "axios";

export const API_BASE_URL = 'https://api2.praguecoolpass.com';

export const api = axios.create({
    baseURL: API_BASE_URL,
});

export const fetchBenefitsData = () => {
    return api.get('/pages/5fd771cc072e5479bded0f2b')
        .then((response) => response.data)
        .catch((error) => {
            console.error('Error fetching data:', error);
            throw error;
        });
};

export const fetchTranslationData = () => {
    return api.get('/translation')
        .then((response) => response.data)
        .catch((error) => {
            console.error('Error fetching data:', error);
            throw error;
        });
};

export const fetchCardCategoriesData = () => {
    return api.get('/cardCategories?eshopId=77a85a2a-6b84-4d79-b856-dfafc14340a0')
        .then((response) => response.data)
        .catch((error) => {
            console.error('Error fetching data:', error);
            throw error;
        });
};

export const fetchAttractionsData = () => {
    return api.get('/object/attraction')
        .then((response) => response.data)
        .catch((error) => {
            console.error('Error fetching data:', error);
            throw error;
        });
};

export const fetchTopAttractionsData = () => {
    return api.get('/object/attraction/top-attractions')
        .then((response) => response.data)
        .catch((error) => {
            console.error('Error fetching data:', error);
            throw error;
        });
};

export const fetchPagesData = () => {
    return api.get('/pages/5fd771cc072e5479bded0f2b')
        .then((response) => response.data)
        .catch((error) => {
            console.error('Error fetching data:', error);
            throw error;
        });
};